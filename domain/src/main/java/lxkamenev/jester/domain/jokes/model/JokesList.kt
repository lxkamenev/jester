package lxkamenev.jester.domain.jokes.model

data class JokesList(
    val error: Boolean,
    val amount: Int,
    var jokes: List<Joke>
)

package lxkamenev.jester.domain.jokes.usecase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import lxkamenev.jester.domain.jokes.repository.JokesRepository

class GetJokes(private val repository: JokesRepository) {

    suspend operator fun invoke(
        categories: List<String>,
        blacklist: List<String>?,
        amount: Int
    ) = withContext(Dispatchers.IO) {
        repository.getJokes(categories, blacklist, amount)
    }
}

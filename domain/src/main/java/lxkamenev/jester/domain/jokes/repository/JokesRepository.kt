package lxkamenev.jester.domain.jokes.repository

import lxkamenev.jester.domain.common.ExecutionResult
import lxkamenev.jester.domain.jokes.model.JokesList

interface JokesRepository {

    fun getJokes(
        categories: List<String>,
        blacklist: List<String>?,
        amount: Int
    ): ExecutionResult<JokesList>
}

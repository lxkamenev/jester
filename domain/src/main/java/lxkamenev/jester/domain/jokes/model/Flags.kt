package lxkamenev.jester.domain.jokes.model

data class Flags(
    val explicit: Boolean = false,
    val nsfw: Boolean = false,
    val political: Boolean = false,
    val racist: Boolean = false,
    val religious: Boolean = false,
    val sexist: Boolean = false
)

package lxkamenev.jester.domain.jokes.model

sealed class Joke {

    abstract val id: Int
    abstract val category: String?
    abstract val error: Boolean
    abstract val flags: Flags?
    abstract val lang: String?
    abstract val safe: Boolean

    data class SinglePartJoke(
        override val id: Int,
        override val category: String?,
        override val error: Boolean,
        override val flags: Flags?,
        override val lang: String?,
        override val safe: Boolean,
        val joke: String?,
    ) : Joke()

    data class MultiPartJoke(
        override val id: Int,
        override val category: String?,
        override val error: Boolean,
        override val flags: Flags?,
        override val lang: String?,
        override val safe: Boolean,
        val setup: String?,
        val delivery: String?
    ) : Joke()

    data class Loader(
        override val id: Int,
        override val category: String? = null,
        override val error: Boolean = false,
        override val flags: Flags? = null,
        override val lang: String? = null,
        override val safe: Boolean = true
    ) : Joke()
}

package lxkamenev.jester.domain.koin

import lxkamenev.jester.domain.jokes.usecase.GetJokes
import org.koin.dsl.module

val koinDomainModule = module {

    single { GetJokes(get()) }
}

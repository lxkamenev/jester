package lxkamenev.jester.domain.common

sealed class ExecutionResult<out T> {
    data class Success<out T>(val data: T) : ExecutionResult<T>()
    data class ApiError(
        val code: Int? = null,
        val modifiedResponse: String? = null
    ) : ExecutionResult<Nothing>()

    data class NetworkError(val errorMessage: String?) : ExecutionResult<Nothing>()
}

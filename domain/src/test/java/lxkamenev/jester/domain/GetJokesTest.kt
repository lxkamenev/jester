package lxkamenev.jester.domain

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import lxkamenev.jester.domain.common.ExecutionResult
import lxkamenev.jester.domain.jokes.model.JokesList
import lxkamenev.jester.domain.jokes.repository.JokesRepository
import lxkamenev.jester.domain.jokes.usecase.GetJokes
import org.junit.Test

class GetJokesTest {
    private val repository: JokesRepository = mockk()
    private val getJokes = GetJokes(repository)

    @Test
    fun `get jokes - success`() = runBlocking {
        // GIVEN
        coEvery {
            repository.getJokes(listOf(), listOf(), 1)
        } returns ExecutionResult.Success(JokesList(false, 1, listOf()))

        // WHEN
        getJokes(listOf(), listOf(), 1)

        // THEN
        coVerify { repository.getJokes(listOf(), listOf(), 1) }
    }

    @Test
    fun `get jokes - api error`() = runBlocking {
        // GIVEN
        coEvery {
            repository.getJokes(listOf(), listOf(), 1)
        } returns ExecutionResult.ApiError()

        // WHEN
        getJokes(listOf(), listOf(), 1)

        // THEN
        coVerify { repository.getJokes(listOf(), listOf(), 1) }
    }

    @Test
    fun `get jokes - network error`() = runBlocking {
        // GIVEN
        coEvery {
            repository.getJokes(listOf(), listOf(), 1)
        } returns ExecutionResult.NetworkError("error")

        // WHEN
        getJokes(listOf(), listOf(), 1)

        // THEN
        coVerify { repository.getJokes(listOf(), listOf(), 1) }
    }
}

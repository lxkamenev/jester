package lxkamenev.jester

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.justRun
import io.mockk.mockk
import lxkamenev.jester.app.jokes.model.JokesMapper
import lxkamenev.jester.app.jokes.model.JokesState
import lxkamenev.jester.app.jokes.viewmodel.JokesViewModel
import lxkamenev.jester.app.util.MINIMUM_LIST_SIZE
import lxkamenev.jester.common.CommonViewModelTest
import lxkamenev.jester.domain.common.ExecutionResult
import lxkamenev.jester.domain.jokes.model.Flags
import lxkamenev.jester.domain.jokes.model.Joke
import lxkamenev.jester.domain.jokes.model.JokesList
import lxkamenev.jester.domain.jokes.usecase.GetJokes
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test

class JokesViewModelTest : CommonViewModelTest(HashMap()) {

    private val getJokes: GetJokes = mockk()
    private val mapper: JokesMapper = mockk()

    private val viewModel = JokesViewModel(getJokes, mapper)

    @Before
    fun setupTest() {
        viewModel.keepHistory()
    }

    @Test
    fun `get jokes - success`() {
        // GIVEN
        coEvery { mapper.buildJokesCollections(genResponse().jokes) } returns genResponse().jokes
        coEvery {
            getJokes(
                listOf("Programming", "Pun"),
                blacklist = null,
                MINIMUM_LIST_SIZE
            )
        } returns ExecutionResult.Success(genResponse())

        // WHEN
        viewModel.jokes(
            listOf("Programming", "Pun"),
            blacklist = null,
            MINIMUM_LIST_SIZE
        )

        // THEN
        coVerify {
            getJokes(
                listOf("Programming", "Pun"),
                blacklist = null,
                MINIMUM_LIST_SIZE
            )
        }

        viewModel.state.hits() shouldBeEqualTo 2
        viewModel.state.value shouldBeInstanceOf JokesState.JokesLoaded::class.java
        (viewModel.state.value as JokesState.JokesLoaded).jokes.jokes[0] shouldBeInstanceOf Joke.MultiPartJoke::class.java
    }

    @Test
    fun `get jokes - api error`() {
        // GIVEN
        justRun { mapper.buildJokesCollections(genResponse().jokes) }
        coEvery {
            getJokes(
                listOf("Programming", "Pun"),
                blacklist = null,
                MINIMUM_LIST_SIZE
            )
        } returns ExecutionResult.ApiError(400, "api error")

        // WHEN
        viewModel.jokes(
            listOf("Programming", "Pun"),
            blacklist = null,
            MINIMUM_LIST_SIZE
        )

        // THEN
        coVerify {
            getJokes(
                listOf("Programming", "Pun"),
                blacklist = null,
                MINIMUM_LIST_SIZE
            )
        }

        viewModel.state.hits() shouldBeEqualTo 2
        viewModel.state.value shouldBeInstanceOf JokesState.Error::class.java
        (viewModel.state.value as JokesState.Error).message shouldBeEqualTo "api error"
    }

    @Test
    fun `get jokes - network error`() {
        // GIVEN
        justRun { mapper.buildJokesCollections(genResponse().jokes) }
        coEvery {
            getJokes(
                listOf("Programming", "Pun"),
                blacklist = null,
                MINIMUM_LIST_SIZE
            )
        } returns ExecutionResult.NetworkError("network error")

        // WHEN
        viewModel.jokes(
            listOf("Programming", "Pun"),
            blacklist = null,
            MINIMUM_LIST_SIZE
        )

        // THEN
        coVerify {
            getJokes(
                listOf("Programming", "Pun"),
                blacklist = null,
                MINIMUM_LIST_SIZE
            )
        }

        viewModel.state.hits() shouldBeEqualTo 2
        viewModel.state.value shouldBeInstanceOf JokesState.Error::class.java
        (viewModel.state.value as JokesState.Error).message shouldBeEqualTo "network error"
    }

    private fun genResponse(): JokesList = JokesList(
        false, 1,
        listOf(
            Joke.MultiPartJoke(
                id = 1,
                category = "category",
                flags = Flags(),
                lang = "en",
                safe = true,
                setup = "setup",
                delivery = "delivery",
                error = false
            )
        )
    )
}

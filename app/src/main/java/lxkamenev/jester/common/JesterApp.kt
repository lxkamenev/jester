package lxkamenev.jester.common

import android.app.Application
import lxkamenev.jester.common.koin.koinAppModule
import lxkamenev.jester.data.koin.koinDataModule
import lxkamenev.jester.domain.koin.koinDomainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class JesterApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@JesterApp)
            modules(listOf(koinAppModule, koinDataModule, koinDomainModule))
        }
    }
}

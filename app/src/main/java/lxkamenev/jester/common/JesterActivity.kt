package lxkamenev.jester.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import lxkamenev.jester.databinding.ActivityJesterLayoutBinding

class JesterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityJesterLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJesterLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}

package lxkamenev.jester.common.koin

import lxkamenev.jester.app.jokes.model.JokesMapper
import lxkamenev.jester.app.jokes.viewmodel.JokesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val koinAppModule = module {

    viewModel { JokesViewModel(get(), get()) }

    single { JokesMapper() }
}

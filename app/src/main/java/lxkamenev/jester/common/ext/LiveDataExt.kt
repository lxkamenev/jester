package lxkamenev.jester.common.ext

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import lxkamenev.jester.common.LiveEvent

/**
 * Sets lambda to observe NON nullable value changes in [Fragment]
 */
fun <T> LiveData<T>.bind(fragment: Fragment, observer: (T) -> Unit) {
    // Ignore coming null values
    this.observe(fragment.viewLifecycleOwner, { it?.run { observer(it) } })
}

/**
 * Sets lambda to observe NON nullable value changes in [AppCompatActivity]
 */
fun <T> LiveData<T>.bind(activity: AppCompatActivity, observer: (T) -> Unit) {
    // Ignore coming null values
    this.observe(activity, { it?.run { observer(it) } })
}

fun <T> LiveEvent<T>.bind(fragment: Fragment, observer: (T) -> Unit) {
    this.observe(fragment.viewLifecycleOwner, { it.run { observer(it) } })
}

/**
 * Sets lambda to observe NON nullable value changes in [AppCompatActivity]
 */
fun <T> LiveEvent<T>.bind(activity: AppCompatActivity, observer: (T) -> Unit) {
    this.observe(activity, { it.run { observer(it) } })
}

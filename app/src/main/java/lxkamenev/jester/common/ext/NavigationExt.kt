package lxkamenev.jester.app.common.ext

import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavDirections

fun NavController.navigateSafe(
    @IdRes resId: Int
) {
    val action = currentDestination?.getAction(resId) ?: graph.getAction(resId)
    if (action != null && currentDestination?.id != action.destinationId) {
        navigate(resId)
    }
}

fun NavController.navigateSafe(
    @IdRes resId: Int,
    navDirections: NavDirections
) {
    val action = currentDestination?.getAction(resId) ?: graph.getAction(resId)
    if (action != null && currentDestination?.id != action.destinationId) {
        navigate(navDirections)
    }
}

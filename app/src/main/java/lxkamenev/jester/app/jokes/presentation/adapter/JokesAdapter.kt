package lxkamenev.jester.app.jokes.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import lxkamenev.jester.R
import lxkamenev.jester.app.jokes.presentation.adapter.vh.JokeVH
import lxkamenev.jester.app.jokes.presentation.adapter.vh.LoaderVH
import lxkamenev.jester.app.jokes.presentation.adapter.vh.MultipleJokeVH
import lxkamenev.jester.app.jokes.presentation.adapter.vh.SingleJokeVH
import lxkamenev.jester.domain.jokes.model.Joke

typealias OnLoadMoreJokes = () -> Unit

class JokesAdapter(private val onLoadMoreJokes: OnLoadMoreJokes) :
    ListAdapter<Joke, JokeVH>(JokesItemDiff()) {

    enum class ViewType {
        SINGLE,
        MULTIPLE,
        LOADER
    }

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is Joke.SinglePartJoke -> ViewType.SINGLE.ordinal
        is Joke.MultiPartJoke -> ViewType.MULTIPLE.ordinal
        is Joke.Loader -> ViewType.LOADER.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokeVH {
        val inflater = LayoutInflater.from(parent.context)
        return when (ViewType.values()[viewType]) {
            ViewType.SINGLE -> SingleJokeVH(
                itemView = inflater.inflate(
                    R.layout.list_item_joke_single,
                    parent,
                    false
                )
            )
            ViewType.MULTIPLE -> MultipleJokeVH(
                itemView = inflater.inflate(
                    R.layout.list_item_joke_multiple,
                    parent,
                    false
                )
            )
            ViewType.LOADER -> LoaderVH(
                itemView = inflater.inflate(
                    R.layout.list_item_joke_loader,
                    parent,
                    false
                ),
                onLoadMoreJokes = onLoadMoreJokes
            )
        }
    }

    override fun onBindViewHolder(holder: JokeVH, position: Int) {
        when (holder) {
            is SingleJokeVH -> holder.bind(getItem(position) as Joke.SinglePartJoke)
            is MultipleJokeVH -> holder.bind(getItem(position) as Joke.MultiPartJoke)
            is LoaderVH -> holder.bind()
        }
    }
}

internal class JokesItemDiff : DiffUtil.ItemCallback<Joke>() {

    override fun areItemsTheSame(oldItem: Joke, newItem: Joke): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Joke, newItem: Joke): Boolean {
        return oldItem.id == newItem.id
    }
}

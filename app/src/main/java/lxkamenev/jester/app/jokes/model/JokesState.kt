package lxkamenev.jester.app.jokes.model

import lxkamenev.jester.domain.jokes.model.JokesList

sealed class JokesState {

    data class JokesLoaded(val jokes: JokesList) : JokesState()
    data class Error(val message: String) : JokesState()
}

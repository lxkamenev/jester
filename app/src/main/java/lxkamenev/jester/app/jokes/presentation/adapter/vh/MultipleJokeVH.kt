package lxkamenev.jester.app.jokes.presentation.adapter.vh

import android.view.View
import lxkamenev.jester.databinding.ListItemJokeMultipleBinding
import lxkamenev.jester.domain.jokes.model.Joke

class MultipleJokeVH(itemView: View) : JokeVH(itemView) {

    private val binding = ListItemJokeMultipleBinding.bind(itemView)

    fun bind(joke: Joke.MultiPartJoke) = with(binding) {
        setup.text = joke.setup
        delivery.text = joke.delivery
    }
}

package lxkamenev.jester.app.jokes.presentation.adapter.vh

import android.view.View
import lxkamenev.jester.app.jokes.presentation.adapter.OnLoadMoreJokes
import lxkamenev.jester.databinding.ListItemJokeLoaderBinding

class LoaderVH(itemView: View, private val onLoadMoreJokes: OnLoadMoreJokes) : JokeVH(itemView) {

    private val binding = ListItemJokeLoaderBinding.bind(itemView)

    fun bind() {
        onLoadMoreJokes()
    }
}

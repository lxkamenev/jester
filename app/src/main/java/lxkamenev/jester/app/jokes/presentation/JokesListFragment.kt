package lxkamenev.jester.app.jokes.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import lxkamenev.jester.R
import lxkamenev.jester.app.jokes.model.JokesState
import lxkamenev.jester.app.jokes.presentation.adapter.JokesAdapter
import lxkamenev.jester.app.jokes.viewmodel.JokesViewModel
import lxkamenev.jester.app.util.MINIMUM_LIST_SIZE
import lxkamenev.jester.common.ext.bind
import lxkamenev.jester.databinding.FragmentJokesListBinding
import lxkamenev.jester.domain.jokes.model.Joke
import org.koin.androidx.viewmodel.ext.android.viewModel

class JokesListFragment : Fragment() {

    private val viewModel: JokesViewModel by viewModel()
    private val jokesAdapter = JokesAdapter {
        viewModel.jokes(listOf("Programming", "Pun"), blacklist = null, MINIMUM_LIST_SIZE)
    }
    private lateinit var binding: FragmentJokesListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_jokes_list, container, false)
        binding = FragmentJokesListBinding.bind(view)

        with(binding) {

            jokesRecycler.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = jokesAdapter
            }
        }

        viewModel.state.bind(this) { state ->
            when (state) {
                is JokesState.JokesLoaded -> {
                    val currentList = jokesAdapter.currentList
                        .filterNot { it is Joke.Loader }
                        .toMutableList()

                    currentList.addAll(state.jokes.jokes)
                    jokesAdapter.submitList(currentList)
                }
                is JokesState.Error -> {
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_LONG).show()
                }
            }
        }

        return view
    }
}

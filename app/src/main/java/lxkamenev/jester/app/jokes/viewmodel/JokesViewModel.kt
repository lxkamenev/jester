package lxkamenev.jester.app.jokes.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import lxkamenev.jester.app.jokes.model.JokesMapper
import lxkamenev.jester.app.jokes.model.JokesState
import lxkamenev.jester.app.util.API_ERROR
import lxkamenev.jester.app.util.MINIMUM_LIST_SIZE
import lxkamenev.jester.app.util.NETWORK_ERROR
import lxkamenev.jester.common.CommonViewModel
import lxkamenev.jester.domain.common.ExecutionResult
import lxkamenev.jester.domain.jokes.usecase.GetJokes

class JokesViewModel(
    private val getJokes: GetJokes,
    private val jokesMapper: JokesMapper
) : CommonViewModel() {

    private val _state = MutableLiveData<JokesState>()
    val state: LiveData<JokesState>
        get() = _state

    init {
        jokes(listOf("Programming", "Pun"), blacklist = null, MINIMUM_LIST_SIZE)
    }

    fun jokes(categories: List<String>, blacklist: List<String>?, amount: Int) =
        viewModelScope.launch {
            _state.value = when (val result = getJokes(categories, blacklist, amount)) {
                is ExecutionResult.Success -> {
                    result.data.jokes = jokesMapper.buildJokesCollections(result.data.jokes)
                    JokesState.JokesLoaded(result.data)
                }
                is ExecutionResult.ApiError -> JokesState.Error(
                    result.modifiedResponse ?: API_ERROR
                )
                is ExecutionResult.NetworkError -> JokesState.Error(
                    result.errorMessage ?: NETWORK_ERROR
                )
            }
        }
}

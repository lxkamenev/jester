package lxkamenev.jester.app.jokes.model

import lxkamenev.jester.app.util.MINIMUM_LIST_SIZE
import lxkamenev.jester.domain.jokes.model.Joke

class JokesMapper {

    fun buildJokesCollections(jokes: List<Joke>): List<Joke> {
        return addLoader(jokes)
    }

    private fun addLoader(jokes: List<Joke>): List<Joke> {
        val modifiedList = jokes.toMutableList()

        // MINIMUM_LIST_SIZE is needed in order not ot add a Loader since if
        // the list is very small and bind() method in its ViewHolder will not be called

        if (modifiedList.size >= MINIMUM_LIST_SIZE)
            modifiedList += Joke.Loader(-1)
        return modifiedList
    }
}

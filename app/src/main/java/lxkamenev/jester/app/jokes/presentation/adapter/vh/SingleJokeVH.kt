package lxkamenev.jester.app.jokes.presentation.adapter.vh

import android.view.View
import lxkamenev.jester.databinding.ListItemJokeSingleBinding
import lxkamenev.jester.domain.jokes.model.Joke

class SingleJokeVH(itemView: View) : JokeVH(itemView) {

    private val binding = ListItemJokeSingleBinding.bind(itemView)

    fun bind(joke: Joke.SinglePartJoke) = with(binding) {
        singleJoke.text = joke.joke
    }
}

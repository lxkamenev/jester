package lxkamenev.jester.data

import lxkamenev.jester.data.common.BaseApiTest
import lxkamenev.jester.data.jokes.entity.JokeEntity
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEndWith
import org.amshove.kluent.shouldStartWith
import org.junit.Test

class JesterRemoteTest : BaseApiTest() {

    @Test
    fun `get jokes`() {
        // GIVEN
        server.enqueue(body("get_jokes.json"))

        // WHEN
        val result = api.getJokes("Programming, Pun", null, 10).execute()

        // THEN
        server.takeRequest().apply {
            method shouldBeEqualTo "GET"
            path?.shouldEndWith("/joke/Programming,%20Pun?amount=10")
        }

        result.body()!!.apply {

            this.jokes!!.apply {
                this.first() shouldBeInstanceOf JokeEntity.SinglePartJokeEntity::class.java
                this.last() shouldBeInstanceOf JokeEntity.MultiPartJokeEntity::class.java

                this.first().apply {
                    id shouldBeEqualTo 33
                    (this as JokeEntity.SinglePartJokeEntity).joke!! shouldStartWith "Today I learned that changing"
                }
            }
        }
    }
}

package lxkamenev.jester.data

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import lxkamenev.jester.data.common.JesterApi
import lxkamenev.jester.data.common.apiErrorCall
import lxkamenev.jester.data.common.networkErrorCall
import lxkamenev.jester.data.common.successCall
import lxkamenev.jester.data.jokes.entity.JokeEntity
import lxkamenev.jester.data.jokes.entity.JokesListEntity
import lxkamenev.jester.data.jokes.repository.JokesRepositoryImpl
import lxkamenev.jester.domain.common.ExecutionResult
import lxkamenev.jester.domain.jokes.model.Joke
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Test
import java.io.IOException

class JesterRepositoryImplTest {

    private val api: JesterApi = mockk()

    private val repository = JokesRepositoryImpl(api)

    @Test
    fun `get jokes - success`() {
        every { api.getJokes("Programming, Pun", null, 10) } returns successCall(genResponse())

        val resource = repository.getJokes(listOf("Programming, Pun"), null, 10)

        verify { api.getJokes("Programming, Pun", null, 10) }
        resource shouldBeInstanceOf ExecutionResult.Success::class.java
        (resource as ExecutionResult.Success).data.jokes.apply {
            this.first() shouldBeInstanceOf Joke.MultiPartJoke::class.java
            (this.first() as Joke.MultiPartJoke).delivery shouldBeEqualTo "delivery"
        }
    }

    @Test
    fun `get jokes - api error`() {
        every { api.getJokes("Programming, Pun", null, 10) } returns apiErrorCall(500)

        val resource = repository.getJokes(listOf("Programming, Pun"), null, 10)

        verify { api.getJokes("Programming, Pun", null, 10) }
        resource shouldBeInstanceOf ExecutionResult.ApiError::class.java
    }

    @Test
    fun `get jokes - network error`() {
        every { api.getJokes("Programming, Pun", null, 10) } returns networkErrorCall(IOException())

        val resource = repository.getJokes(listOf("Programming, Pun"), null, 10)

        verify { api.getJokes("Programming, Pun", null, 10) }
        resource shouldBeInstanceOf ExecutionResult.NetworkError::class.java
    }

    private fun genResponse(): JokesListEntity = JokesListEntity(
        false, 1,
        listOf(
            JokeEntity.MultiPartJokeEntity(
                setup = "setup",
                delivery = "delivery",
            )
        )
    )
}

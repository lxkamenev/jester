package lxkamenev.jester.data.common

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import lxkamenev.jester.data.jokes.adapter.createJokesTypeAdapter
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStream
import java.util.Scanner

open class BaseApiTest {
    val server = MockWebServer()

    val api: JesterApi by lazy { retrofit.create(JesterApi::class.java) }

    internal open val okHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder().followRedirects(false).build()
    }

    private val retrofit: Retrofit by lazy {
        val gson = GsonBuilder()
            .registerTypeAdapterFactory(createJokesTypeAdapter())
            .create()

        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(server.url("/"))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    fun body(fileName: String): MockResponse {
        return MockResponse().apply { setBody(textFromFile(fileName)) }
    }

    fun error(code: Int, fileName: String): MockResponse {
        return MockResponse().apply {
            setResponseCode(code)
            setBody(textFromFile(fileName))
        }
    }

    private fun Any.textFromFile(fileName: String): String {
        val resourceAsStream = javaClass.classLoader?.getResourceAsStream(fileName)
        return if (resourceAsStream == null) "" else convertStreamToString(resourceAsStream)
    }

    private fun convertStreamToString(inpStream: InputStream): String {
        val s = Scanner(inpStream, "UTF-8").useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }
}

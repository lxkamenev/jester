package lxkamenev.jester.data.common

import io.mockk.every
import io.mockk.mockk
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

fun <T> success(value: T): Response<T> = Response.success(value)

fun <T> error(code: Int): Response<T> {
    val errorBody = "".toResponseBody("application/json".toMediaTypeOrNull())
    return Response.error(code, errorBody)
}

fun <T> successCall(value: T): Call<T> {
    return mockk {
        every { execute() } returns success(value)
    }
}

fun <T> apiErrorCall(code: Int): Call<T> {
    return mockk {
        every { execute() } returns error(code)
    }
}

fun <T> networkErrorCall(ioException: IOException): Call<T> {
    return mockk {
        every { execute() } throws ioException
    }
}

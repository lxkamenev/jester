package lxkamenev.jester.data.koin

import android.util.Log
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import lxkamenev.jester.data.BuildConfig
import lxkamenev.jester.data.common.JesterApi
import lxkamenev.jester.data.jokes.adapter.createJokesTypeAdapter
import lxkamenev.jester.data.jokes.repository.JokesRepositoryImpl
import lxkamenev.jester.domain.jokes.repository.JokesRepository
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://v2.jokeapi.dev/"

val koinDataModule = module {

    single {
        HttpLoggingInterceptor { message -> Log.v("HTTP Logger", message) }.apply {
            level = if (BuildConfig.DEBUG)
                HttpLoggingInterceptor.Level.BODY
            else
                HttpLoggingInterceptor.Level.NONE
        }
    }

    single(named("OkHttpClient")) {
        OkHttpClient.Builder()
            .readTimeout(if (BuildConfig.DEBUG) 120 else 30.toLong(), TimeUnit.SECONDS)
            .connectTimeout(if (BuildConfig.DEBUG) 120 else 30.toLong(), TimeUnit.SECONDS)
            .addInterceptor(get<HttpLoggingInterceptor>())
            .cache(
                Cache(
                    directory = File(androidContext().externalCacheDir, "jester-cache"),
                    maxSize = 10_1485_760
                )
            )
            .build()
    }

    single<JesterApi> {

        val gson = GsonBuilder()
            .registerTypeAdapterFactory(createJokesTypeAdapter())
            .create()

        Retrofit.Builder()
            .client(get(named("OkHttpClient")))
            .baseUrl(BASE_URL) // not the best way but for now it's ok
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(JesterApi::class.java)
    }

    single<JokesRepository> { JokesRepositoryImpl(get()) }
}

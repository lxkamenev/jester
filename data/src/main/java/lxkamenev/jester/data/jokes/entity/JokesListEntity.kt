package lxkamenev.jester.data.jokes.entity

import lxkamenev.jester.domain.jokes.model.JokesList

data class JokesListEntity(
    val error: Boolean,
    val amount: Int,
    val jokes: List<JokeEntity>?
) {
    fun toModel(): JokesList = JokesList(error, amount, jokes?.map { it.toModel() } ?: emptyList())
}

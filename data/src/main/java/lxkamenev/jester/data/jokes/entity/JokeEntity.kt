package lxkamenev.jester.data.jokes.entity

import lxkamenev.jester.domain.jokes.model.Joke

sealed class JokeEntity {

    var type: String? = null
    val category: String? = null
    val error: Boolean = false
    val flags: FlagsEntity? = null
    val id: Int = 0
    val lang: String? = null
    val safe: Boolean = false

    data class SinglePartJokeEntity(
        val joke: String? = null
    ) : JokeEntity()

    data class MultiPartJokeEntity(
        val setup: String? = null,
        val delivery: String? = null
    ) : JokeEntity()

    fun toModel(): Joke = when (this) {
        is SinglePartJokeEntity -> Joke.SinglePartJoke(
            id = id,
            category = category,
            error = error,
            flags = flags?.toModel(),
            lang = lang,
            safe = safe,
            joke = joke
        )
        is MultiPartJokeEntity -> Joke.MultiPartJoke(
            id = id,
            category = category,
            error = error,
            flags = flags?.toModel(),
            lang = lang,
            safe = safe,
            setup = setup,
            delivery = delivery
        )
    }
}

package lxkamenev.jester.data.jokes.adapter

import com.google.gson.TypeAdapterFactory
import lxkamenev.jester.data.common.RuntimeTypeAdapterFactory
import lxkamenev.jester.data.jokes.entity.JokeEntity

const val SINGLE_PART_JOKE = "single"
const val TWO_PART_JOKE = "twopart"

fun createJokesTypeAdapter(): TypeAdapterFactory = RuntimeTypeAdapterFactory
    .of(JokeEntity::class.java, "type")
    .registerSubtype(JokeEntity.SinglePartJokeEntity::class.java, SINGLE_PART_JOKE)
    .registerSubtype(JokeEntity.MultiPartJokeEntity::class.java, TWO_PART_JOKE)

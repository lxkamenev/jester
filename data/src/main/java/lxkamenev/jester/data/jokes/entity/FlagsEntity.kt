package lxkamenev.jester.data.jokes.entity

import lxkamenev.jester.domain.jokes.model.Flags

data class FlagsEntity(
    val explicit: Boolean,
    val nsfw: Boolean,
    val political: Boolean,
    val racist: Boolean,
    val religious: Boolean,
    val sexist: Boolean
) {
    fun toModel(): Flags = Flags(
        explicit = explicit,
        nsfw = nsfw,
        political = political,
        racist = racist,
        religious = religious,
        sexist = sexist
    )
}

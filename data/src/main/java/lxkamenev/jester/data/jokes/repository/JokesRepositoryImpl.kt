package lxkamenev.jester.data.jokes.repository

import lxkamenev.jester.data.common.JesterApi
import lxkamenev.jester.data.common.apiCall
import lxkamenev.jester.domain.common.ExecutionResult
import lxkamenev.jester.domain.jokes.model.JokesList
import lxkamenev.jester.domain.jokes.repository.JokesRepository

class JokesRepositoryImpl(
    private val api: JesterApi
) : JokesRepository {

    override fun getJokes(
        categories: List<String>,
        blacklist: List<String>?,
        amount: Int
    ): ExecutionResult<JokesList> =
        apiCall {
            api.getJokes(
                categories.joinToString(separator = ",") { it },
                blacklist?.joinToString(separator = ",") { it },
                amount
            )
        }.toResource { it.toModel() }
}

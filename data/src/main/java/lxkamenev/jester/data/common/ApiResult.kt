package lxkamenev.jester.data.common

import lxkamenev.jester.domain.common.ExecutionResult
import retrofit2.Call
import java.io.IOException

sealed class ApiResult<T> {
    data class Success<T>(val data: T) : ApiResult<T>()
    data class ApiError<T>(
        val code: Int? = null,
        val modifiedResponse: String? = null
    ) : ApiResult<T>()

    data class NetworkError<T>(val errorMessage: String?) : ApiResult<T>()

    fun <R> toResource(transformSuccess: (data: T) -> R): ExecutionResult<R> = when (this) {
        is Success -> ExecutionResult.Success(transformSuccess(data))
        is ApiError -> ExecutionResult.ApiError(code, modifiedResponse)
        is NetworkError -> ExecutionResult.NetworkError(errorMessage)
    }
}

@Suppress("UNCHECKED_CAST")
internal fun <T> apiCall(apiCall: () -> Call<T>): ApiResult<T> {
    val response = try {
        apiCall().execute()
    } catch (e: IOException) {
        return ApiResult.NetworkError(e.message)
    }

    return if (response.isSuccessful) {
        val body = response.body()
        if (body == null) {
            ApiResult.Success(null as T)
        } else {
            ApiResult.Success(body)
        }
    } else {
        ApiResult.ApiError(
            code = response.code(),
            modifiedResponse = response.errorBody().toString()
        )
    }
}

package lxkamenev.jester.data.common

import lxkamenev.jester.data.jokes.entity.JokesListEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface JesterApi {

    @GET("joke/{categories}")
    fun getJokes(
        @Path("categories") categoriesString: String,
        @Query("blacklistFrags") blacklistFlags: String?,
        @Query("amount") amount: Int,
    ): Call<JokesListEntity>
}
